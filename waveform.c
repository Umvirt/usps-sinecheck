#include <stdio.h> 
#include <sndfile.h>
#include <math.h>

#define	BLOCK_SIZE 4096

int main(int argc, char * argv []){
//printf("hi");

SNDFILE * fh;
FILE * ofh, * o2fh;

SF_INFO info;
info.format=0;

fh=sf_open    (argv[1], SFM_READ, &info) ;
ofh = fopen("1.txt","w");
o2fh = fopen("2.txt","w");

printf("Frames %ld\nRate: %d\nFormat: %d\nSections:%d\nSeekable: %d\n\n",info.frames,info.samplerate,info.format,info.sections,info.seekable);


float buf [BLOCK_SIZE] ;
	sf_count_t frames ;
	int rise, pikes, c,k, m, readcount, limit ;
	float max,last,lmax,diff, threshold;

//	bool flip;

	threshold=1/(float)1000;

	limit=info.samplerate;
	
	printf("Limit: %d\n\n",limit);
	frames = BLOCK_SIZE;

	c=0;
pikes=0;
	while ((readcount = sf_readf_float (fh, buf, frames)) > 0)
	{	for (k = 0 ; k < readcount && c< limit ; k++)
		{	//for (m = 0 ; m < channels ; m++)
				//if (full_precision)
				//	fprintf (outfile, " %.*e", OP_DBL_Digs - 1, buf [k * channels + m]) ;
				//else
				//	fprintf (outfile, " % 12.10f", buf [k * channels + m]) ;
			//fprintf (outfile, "\n") ;
			//} ;
	
			fprintf (ofh,"%d\t % 12.10f\n",c, buf [k]);
			if(c<limit/50){
				fprintf (o2fh,"%d\t % 12.10f\n",c, buf [k]);
			}
			//max
			if(c==0){
			max=buf[k];
			}else{
				if(max<buf[k]){
					max=buf[k];
				}
			}



			//pikes
			if(c>5 && (fmod(c,1)==0)){
			//if(c>5){

			diff=fabs(last-buf[k]);
//			flip=(last/buf[k]==-1);

//			printf("% 12.10f = % 12.10f \\ % 12.10f\n",buf[k], diff,threshold);			
//if(flip){
//printf("flip\n");
//}

			//rise
			if(buf[k]>last && (diff>threshold || -diff>threshold)){
			lmax=buf[k];
			rise=1;
			}

			//fall
			if(buf[k]<last && (diff>threshold || -diff>threshold)){
	if(rise){
			pikes++;
//printf("pike\n");
			rise=0;
	}	
		lmax=buf[k];
			}


			}


                        //last
                        last=buf[k];


			c++;





		} ;
	
		if(c>limit+5){
		break;
		}
	}


sf_close(fh);
fclose(ofh);
fclose(o2fh);

printf ("max: % 12.10f\n", max);
printf ("frequency: %d\n", (pikes));


}
